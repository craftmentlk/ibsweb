import React,{useEffect}  from 'react';
import Color from 'color';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { useFourThreeCardMediaStyles } from '@mui-treasury/styles/cardMedia/fourThree';
import AOS from "aos";
import "aos/dist/aos.css";
import img1 from '../../Assets/bg1.jpg';
import img2 from '../../Assets/bg2.jpg';

const useStylesx = makeStyles((theme) => ({
  root: {
   
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  rootgrid:{
  },
  title: {
    fontSize: '3rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  colorText: {
    textAlign: 'center',
    color: '#4472c4',
  },
  compGrid:{
    justifyContent: 'center',
    padding:"2%"
  },
  compGrid2:{
    marginTop:'5%',
    verticalAlign:'center',
    justifyContent: 'center',
    alignContent:'center',
    padding:"2%",
  },
  minititle: {
    fontSize: '1rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  listtitle: {
    color: '#4472c4'
  },
}));

const useGridStyles = makeStyles(({ breakpoints }) => ({
  root: {
    [breakpoints.up('md')]: {
      justifyContent: 'center',
    }
  }
}));

const useStyles = makeStyles(() => ({
  actionArea: {
    borderRadius: 16,
    transition: '0.2s',
    '&:hover': {
      transform: 'scale(1.1)',
    },
  },
  card: ({ color }) => ({
    minWidth: 256,
    borderRadius: 16,
    boxShadow: 'none',
    '&:hover': {
      boxShadow: `0 6px 12px 0 ${Color(color)
        .rotate(-12)
        .darken(0.2)
        .fade(0.5)}`,
    },
  }),
  content: ({ color }) => {
    return {
      backgroundColor: color,
      padding: '1rem 1.5rem 1.5rem',
    };
  },
  title: {
    fontFamily: 'Keania One',
    fontSize: '2rem',
    color: '#fff',
    textTransform: 'uppercase',
  },
  subtitle: {
    fontFamily: 'Montserrat',
    color: '#fff',
    opacity: 0.87,
    marginTop: '2rem',
    fontWeight: 500,
    fontSize: 14,
  },
}));

const CustomCard = ({ classes, image, title, subtitle }) => {
  const mediaStyles = useFourThreeCardMediaStyles();
  return (
    <CardActionArea className={classes.actionArea}>
      <Card className={classes.card}>
        <CardMedia classes={mediaStyles} image={image} />
        <CardContent className={classes.content}>
          <Typography className={classes.title} variant={'h2'}>
            {title}
          </Typography>
          <Typography className={classes.subtitle}>{subtitle}</Typography>
        </CardContent>
      </Card>
    </CardActionArea>
  );
};

export const SolidGameCardDemobim = React.memo(function SolidGameCard() {

  const gridStyles = useGridStyles();
  const classes = useStylesx();
  useEffect(() => {
    AOS.init({
      ...AOS,
      duration:2000
    });
    AOS.refresh();
  }, []);
  const styles = useStyles({ color: '#203f52' });
  const styles2 = useStyles({ color: '#4d137f' });
  const styles3 = useStyles({ color: '#ff9900' });
  const styles4 = useStyles({ color: '#34241e' });
  return (
    <>
       <div id="bim">
      

              <Grid container justify='center'>
                  <Grid data-aos="fade-right" className={classes.compGrid} item xs={12} sm={4}>
                      <h1 className={classes.title}>
                          BIM <span className={classes.colorText}>Services</span>
                      </h1>
                      <p className={classes.minititle} >
                          Engineers can use CAD technologies to create various elements of an overall design in a precise and detailed manner. BIM Intelligence provides AutoCAD drafting, PDF to AutoCAD conversion, modifying the client's AutoCAD files, bespoke automated AutoCAD tools and exporting Revit projects as AutoCAD files.”</p>

                  </Grid>

                  <Grid data-aos="fade-left" className={classes.compGrid2} item xs={12} sm={8} >
                     
                  </Grid>

                  <Grid data-aos="fade-up" classes={gridStyles} container spacing={4} wrap={'nowrap'}>
                          <Grid item>
                              <CustomCard
                                  classes={styles}
                                  title={<h6>Mechanical</h6>}
                                  subtitle={'Be a Legend!'}
                                  image={img1}
                              />
                          </Grid>
                          <Grid item>
                              <CustomCard
                                  classes={styles2}
                                  title={<h6>Electrical</h6>}
                                  subtitle={'Time to choose side!'}
                                  image={img2}
                              />
                          </Grid>
                          <Grid item>
                              <CustomCard
                                  classes={styles3}
                                  title={<h6>Plumbing</h6>}
                                  subtitle={'What are you waiting?'}
                                  image={img1}
                              />
                          </Grid>
                          <Grid item>
                              <CustomCard
                                  classes={styles4}
                                  title={<h6>Common</h6>}
                                  subtitle={'What are you waiting?'}
                                  image={img1}
                              />
                          </Grid>
                          <Grid item>
                              <CustomCard
                                  classes={styles4}
                                  title={<h6>Special</h6>}
                                  subtitle={'What are you waiting?'}
                                  image={img1}
                              />
                          </Grid>
                         
                      </Grid>
              </Grid>


    
      <br/>
      </div>
    </>
  );
});
export default SolidGameCardDemobim