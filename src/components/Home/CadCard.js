import React,{useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AOS from "aos";
import "aos/dist/aos.css";
import Lottie from 'react-lottie';
import animationData from '../../Lotties/aboutus.json';

const useStyles = makeStyles((theme) => ({
  root: {
   
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  rootgrid:{
  },
  title: {
    fontSize: '3rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  colorText: {
    textAlign: 'center',
    color: '#4472c4',
  },
  compGrid:{
    justifyContent: 'center',
    padding:"2%"
  },
  compGrid2:{
    marginTop:'5%',
    verticalAlign:'center',
    justifyContent: 'center',
    alignContent:'center',
    padding:"2%",
  },
  minititle: {
    fontSize: '1rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  listtitle: {
    color: '#4472c4'
  },
}));

export default function () {
  const classes = useStyles();
  useEffect(() => {
    AOS.init({
      ...AOS,
      duration:2000
    });
    AOS.refresh();
  }, []);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };


  return (
      <div className={classes.root} id="cad">

          <Grid className={classes.rootgrid} container justify='center'>

              <Grid data-aos="fade-right" className={classes.compGrid} item xs={12} sm={5}>

                  <Lottie
                      options={defaultOptions}
                      height='auto'
                      width='auto'
                  />
              </Grid>
              <Grid data-aos="fade-left" className={classes.compGrid2} item xs={12} sm={6} >

                  <h1 className={classes.title}>
                      CAD <span className={classes.colorText}> Services</span>
                  </h1>

              </Grid>
          </Grid>
      </div>
  );
}