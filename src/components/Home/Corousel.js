import React, { Fragment, useEffect, useState } from 'react';
//import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
//import animationData from '../../Lotties/city.json';
import Carousel from 'react-material-ui-carousel';
import Stage1 from './Stage1'
import {Card1,Card2} from './Stage2'
// import { useBlogTextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/blog';
// import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';



// const useStyles = makeStyles(({ breakpoints, spacing }) => ({
//     container: {
//       textAlign: 'center',
//       fontFamily: 'Nunito',
//     },
//     title: {
//         fontSize: '2.5rem',
//         color: '#002060'
//     },
//     colorText: {
//         textAlign: 'center',
//         color: '#4472c4',
//       },
//     minititle: {
//         fontSize: '1.2rem',
//         color: '#002060'
//       },
//        corousel:{
//         width:'80%'
//     } ,
// }));

export default function Corousel() {
   // const classes = useStyles();

    useEffect(() => {
      // setChecked(true);
    }, []);


    // const defaultOptions = {
    //     loop: true,
    //     autoplay: true,
    //     animationData: animationData,
    //     rendererSettings: {
    //       preserveAspectRatio: "xMidYMid slice"
    //     }
    //   };

  const [screenSize, getDimension] = useState({
    dynamicWidth: window.innerWidth,
    dynamicHeight: window.innerHeight
  });
 
  const setDimension = () => {
    getDimension({
      dynamicWidth: window.innerWidth,
      dynamicHeight: window.innerHeight
    })
  }
   
  useEffect(() => {
    window.addEventListener('resize', setDimension);
     
    return(() => {
        window.removeEventListener('resize', setDimension);
    })
  }, [screenSize])

    return(
        <Fragment>
            <Grid container justify='center'>
            <Carousel interval={8000} indicators={true} >
                
                <Stage1 />
                <Card1 />
                <Card2/>
               
        </Carousel>
            </Grid>

        </Fragment>    
    );
};