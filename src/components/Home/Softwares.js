import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  
  title: {
    fontSize: '3rem',
    //marginTop: -220,
    color: '#002060',
    fontFamily: 'Nunito',
  },
  colorText: {
    textAlign: 'center',
    color: '#4472c4',
  },
}));
export default function () {
  const classes = useStyles();
  return (
    <div className={classes.root} id="soft">
        <h1 className={classes.title}>
            Soft<span className={classes.colorText}>wares</span>
          </h1>
      {/* <ImageCard place={places[1]} checked={checked} />
      <ImageCard place={places[0]} checked={checked} /> */}
    </div>
  );
}