import React,{useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AOS from "aos";
import "aos/dist/aos.css";
import Lottie from 'react-lottie';
import animationData from '../../Lotties/aboutus.json';

const useStyles = makeStyles((theme) => ({
  root: {
   
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  rootgrid:{
  },
  title: {
    fontSize: '3rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  colorText: {
    textAlign: 'center',
    color: '#4472c4',
  },
  compGrid:{
    justifyContent: 'center',
    padding:"2%"
  },
  compGrid2:{
    marginTop:'5%',
    verticalAlign:'center',
    justifyContent: 'center',
    alignContent:'center',
    padding:"2%",
  },
  minititle: {
    fontSize: '1rem',
    color: '#002060',
    fontFamily: 'Nunito',
  },
  listtitle: {
    color: '#4472c4'
  },
}));

export default function CadServices () {
  const classes = useStyles();
  useEffect(() => {
    AOS.init({
      ...AOS,
      duration:2000
    });
    AOS.refresh();
  }, []);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };


  return (
    <div  className={classes.root} id="about">

      <Grid  className={classes.rootgrid} container justify='center'>

        <Grid data-aos="fade-right" className={classes.compGrid} item xs={12} sm={5}>
          <h1 className={classes.title}>
            About<span className={classes.colorText}>US</span>
          </h1>
          <p className={classes.minititle} >
            BIM Intelligence INC is a group of BIM and CAD experts based in Canada.
            We streamline the building design and construction processes to decrease project time and expense while improving project quality.
            Our goal is to create a shared virtual BIM model of a project that will allow building designers, contractors, owners, and facilities managers to all have a common understanding of the project.</p>

          <p className={classes.minititle} >
            Before the building phase, clashes between different design disciplines can be discovered and handled in the BIM 3D model.
            Cost estimating, 4D modeling, energy analysis, simulations ,facility management, virtual reality, augmented reality, and BMS are all possible uses for the BIM model.
            The following services are available from us:
          </p>

          <p className={classes.minititle} >
            The following services are available from us:</p>
          <ul className={classes.listtitle}>
            <li>Provide 2D design and construction drawings in AutoCAD and PDF</li>
            <li> Model the architectural drawings in Autodesk Revit</li>
            <li> Model the structural drawings in Autodesk Revit</li>
            <li>Model the MEP drawings in Autodesk Revit </li>
            <li>Perform clash detection in Navisworks </li>
            <li>Create 4D models in Navisworks</li>
            <li>Creating As Built Revit Models of existing buildings using LIDAR Technology</li>
            <li>Lighting calculation and simulation based on Dialux platform </li>
            <li>Energy optimization of Buildings </li>
            <li>Computational Fluid Dynamics (CFD) Simulations of Spaces, Ducts and Pipes </li>
            <li>Detailed Bill of material of MEP systems </li>
            <li>Site visits on case-by-case basis</li>
            <li>Large Scale BIM MEP Trainings for Companies</li>
            <li>DYNAMO Based Automations for Repetitive tasks and for Extracting Project Data from Revit</li>
            <li>Navisworks Based 3D Model Reviews</li>
            <li>High-Definition Walkthrough Videos for Presentation and Viewing Purposes</li>
             <li>Revit and AutoCAD Automations and Add-ins to Suit Customers’ special Requirements”</li>
          </ul>
        </Grid>
        <Grid  data-aos="fade-left"  className={classes.compGrid2} item xs={12} sm={6} >
        <Lottie
                            options={defaultOptions}
                            height='auto'
                            width='auto'
                        />
        </Grid>
      </Grid>
    </div>
  );
}