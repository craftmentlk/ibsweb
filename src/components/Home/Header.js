import React, { Fragment } from "react";
import { Link as RouterLink } from "react-router-dom";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import { Link as Scroll } from "react-scroll";
import MoreIcon from "@material-ui/icons/MoreVert";
import MenuItem from "@material-ui/core/MenuItem";

import Menu from "@material-ui/core/Menu";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Logo from "../../Assets/Logo.png";
import Corosel from "./Corousel";
import ListItemText from "@material-ui/core/ListItemText";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import AboutUs from "./Aboutus";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "110vh",
    fontFamily: "Nunito",
  },
  rootNav: {
    "& > *": {
      margin: theme.spacing(2),
    },
  },
  logo: {
    maxWidth: 115,
    maxHeight: 115,
  },
  appbar: {
    backgroundColor: "#ffff",
    //background:'none',
    position: "flex",
    // marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
  },
  buttonNav: {
    color: "#4472c4",
    fontWeights: "100",
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },

  appbarWrapper: {
    width: "100%",
    margin: "0 auto",
  },
  appbarTitle: {
    flexGrow: "1",
    color: "#002060",
  },
  icon: {
    color: "#fff",
    fontSize: "2rem",
  },
  colorText: {
    textAlign: "center",
    color: "#4472c4",
  },
  container: {
    textAlign: "center",
    fontFamily: "Nunito",
  },
  containerlottie: {
    textAlign: "center",
  },
  title: {
    fontSize: "2rem",
    color: "#002060",
  },
  minititle: {
    fontSize: "1rem",
    color: "#002060",
  },
  goDown: {
    color: "#4472c4",
    fontSize: "4rem",
  },
  listtext: {
    color: "#4472c4",
    fontFamily: "Nunito",
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
    padding: "1%",
  },
}));

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& .MuiListItemIcon-root, & .MuiListItemText-primary": {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function Header() {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorE3, setAnchorE3] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const [anchorE2, setAnchorE2] = React.useState(null);

  const handleClick = (event) => {
    setAnchorE2(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorE2(null);
  };
  const handleClick1 = (event) => {
    setAnchorE3(event.currentTarget);
  };

  const handleClose1 = () => {
    setAnchorE3(null);
  };
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      aria-setsize={4}
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      style={{ color: "#4472c4" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem className={classes.buttonNav}>
        <p>Home</p>
      </MenuItem>

      <MenuItem className={classes.buttonNav}>
        <p>CAD Services</p>
      </MenuItem>

      {/* <MenuItem className={classes.buttonNav}>
        <p>Softwares</p>
      </MenuItem> */}

      <MenuItem className={classes.buttonNav}>
        <p>About Us</p>
      </MenuItem>

      <MenuItem className={classes.buttonNav}>
        <p>Career</p>
      </MenuItem>
    </Menu>
  );

  return (
    <Fragment>
      <Grid className={classes.root} id="header">
        <AppBar className={classes.appbar} elevation={0}>
          <Toolbar className={classes.appbarWrapper}>
            <h1 className={classes.appbarTitle}>
              <img src={Logo} alt="logo" className={classes.logo} />
            </h1>
            <Grid className={classes.sectionDesktop}>
              <Grid className={classes.rootNav}>
                <Scroll to="header" smooth={true}>
                  <Button className={classes.buttonNav}>Home</Button>
                </Scroll>
                <Scroll to="cad" smooth={true}>
                  <Button
                    className={classes.buttonNav}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    endIcon={<ArrowDropDownIcon />}
                    onClick={handleClick1}
                  >
                    CAD Services
                  </Button>
                </Scroll>

                <StyledMenu
                  id="customized-menu"
                  anchorEl={anchorE3}
                  keepMounted
                  open={Boolean(anchorE3)}
                  onClose={handleClose1}
                >
                  <StyledMenuItem>
                    {" "}
                    <RouterLink to="/cadServices">
                      <ListItemText
                        className={classes.listtext}
                        primary="Design Drawings"
                      />
                    </RouterLink>
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Shop Drawings" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Others" />
                  </StyledMenuItem>
                </StyledMenu>
                <Scroll to="bim" smooth={true}>
                  <Button
                    className={classes.buttonNav}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    endIcon={<ArrowDropDownIcon />}
                    onClick={handleClick}
                  >
                    BIM Services
                  </Button>
                </Scroll>
                <StyledMenu
                  id="customized-menu"
                  anchorEl={anchorE2}
                  keepMounted
                  open={Boolean(anchorE2)}
                  onClose={handleClose}
                >
                  <StyledMenuItem>
                    <ListItemText primary="Mechanical" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Electrical" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Plumbing" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Common Services" />
                  </StyledMenuItem>
                  <StyledMenuItem>
                    <ListItemText primary="Special" />
                  </StyledMenuItem>
                </StyledMenu>

                <Scroll to="about" smooth={true}>
                  <Button className={classes.buttonNav}>About Us</Button>
                </Scroll>
                <Button className={classes.buttonNav}>Career</Button>
              </Grid>
            </Grid>
            <Grid className={classes.sectionMobile}>
              <IconButton
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="primary"
              >
                <MoreIcon />
              </IconButton>
            </Grid>
          </Toolbar>
        </AppBar>

        {renderMobileMenu}
        {renderMenu}
        {/* *****stage1*** */}
        <Corosel />
      </Grid>
      <AboutUs />
    </Fragment>
  );
}
