import React, { Fragment, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Lottie from 'react-lottie';
import animationData from '../../Lotties/cad2.json';

const useStyles = makeStyles(({ breakpoints, spacing }) => ({
    container: {
      textAlign: 'center',
      fontFamily: 'Nunito',
    },
    title: {
        fontSize: '2.5rem',
        color: '#002060'
    },
    colorText: {
        textAlign: 'center',
        color: '#4472c4',
      },
    minititle: {
        fontSize: '1.5rem',
        color: '#002060'
      },
}));

export default function ImageCard() {
    const classes = useStyles();

    useEffect(() => {
      // setChecked(true);
    }, []);


    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: "xMidYMid slice"
        }
      };

  const [screenSize, getDimension] = useState({
    dynamicWidth: window.innerWidth,
    dynamicHeight: window.innerHeight
  });
 
  const setDimension = () => {
    getDimension({
      dynamicWidth: window.innerWidth,
      dynamicHeight: window.innerHeight
    })
  }
   
  useEffect(() => {
    window.addEventListener('resize', setDimension);
     
    return(() => {
        window.removeEventListener('resize', setDimension);
    })
  }, [screenSize])

    return(
        <Fragment>
            <div>
                <Grid container justify='center' >
                    <Grid item xs={12} sm={6} justify='center' >
                        <Lottie
                            options={defaultOptions}
                            height='auto'
                            width='auto'
                        />
                    </Grid>
                </Grid>

                    <Grid className={classes.container}>
                        <h1 className={classes.title}>
                            CAD
                            <span className={classes.colorText}> Services</span>
                        </h1>
                        <p className={classes.minititle}>
                            beyond information modeling..
                        </p>
                    </Grid>
            </div>
        </Fragment>    
    );
};