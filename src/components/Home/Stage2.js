import React, {  Fragment,useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Lottie from 'react-lottie';
import animationData from '../../Lotties/bim1.json';
import animationData2 from '../../Lotties/data2.json';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import TextInfoContent from '@mui-treasury/components/content/textInfo';
//import { useFourThreeCardMediaStyles } from '@mui-treasury/styles/cardMedia/fourThree';
import { useN04TextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/n04';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';

import cx from 'clsx';
const useStyles = makeStyles(({ breakpoints, spacing }) => ({
  container: {
    textAlign: 'center',
    fontFamily: 'Nunito',
  },
  title: {
    fontSize: '2.5rem',
    color: '#002060'
  },
  colorText: {
    textAlign: 'center',
    color: '#4472c4',
  },
  minititle: {
    fontSize: '1.5rem',
    color: '#002060'
  },
  root: {
    maxWidth: 400,
    margin: 'auto',
    borderRadius: 12,
    padding: 12,
  },
  media: {
    borderRadius: 8,
  },
}));


export const MusicCardDemo = React.memo(function MusicCard() {
  const styles = useStyles();
  //const mediaStyles = useFourThreeCardMediaStyles();
  const textCardContentStyles = useN04TextInfoContentStyles();
  const shadowStyles = useOverShadowStyles({ inactive: true });

  useEffect(() => {
    // setChecked(true);
  }, []);


  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };



  return (
    <Card className={cx(styles.root, shadowStyles.root)}>
      <CardMedia>
        <Lottie
          options={defaultOptions}
          height='300px'
          width='300px'
        />
      </CardMedia>
      <CardContent>
        <TextInfoContent
          classes={textCardContentStyles}
          overline={'BIM Modeling'}
          heading={'BIM Modeling'}
          body={
            'That year, collection of songs, review melodies, memories full, this is a long and warm journey'
          }
        />
      </CardContent>
    </Card>
  );
});


export const MusicCardDemo2 = React.memo(function MusicCard() {
  const styles = useStyles();
  //const mediaStyles = useFourThreeCardMediaStyles();
  const textCardContentStyles = useN04TextInfoContentStyles();
  const shadowStyles = useOverShadowStyles({ inactive: true });

  useEffect(() => {
    // setChecked(true);
  }, []);


  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData2,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };



  return (
    <Card className={cx(styles.root, shadowStyles.root)}>
      <CardMedia>
        <Lottie
          options={defaultOptions}
          height='300px'
          width='300px'
        />
      </CardMedia>
      <CardContent>
        <TextInfoContent
          classes={textCardContentStyles}
          overline={'CAD Services'}
          heading={'CAD Services'}
          body={
            'That year, collection of songs, review melodies, memories full, this is a long and warm journey'
          }
        />
      </CardContent>
    </Card>
  );
});



export function Card1() {
  const classes = useStyles();

  
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData2,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };

  
  return(
    <Fragment>
    <div>
    <Grid container justify='center' >
                    <Grid justify='center' alignItems='center' alignContent='center'  item xs={12} sm={6}>
                        <Lottie
                            options={defaultOptions}
                            height='auto'
                            width='auto'
                        />
                    </Grid>
                   </Grid>

                    <Grid className={classes.container}>
                        <h1 className={classes.title}>
                            CAD
                            <span className={classes.colorText}> Services</span>
                        </h1>
                        <p className={classes.minititle}>
                            beyond information modeling..
                        </p>
                    </Grid>
    </div>
</Fragment>    
  )
}

export function Card2() {
  const classes = useStyles();

  
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };

  
  return(
    <Fragment>
    <div>
    <Grid container justify='center' >
                    <Grid justify='center' alignItems='center' alignContent='center'  item xs={12} sm={6}>
                        <Lottie
                            options={defaultOptions}
                            height='auto'
                            width='auto'
                        />
                    </Grid>
                   </Grid>

                    <Grid className={classes.container}>
                        <h1 className={classes.title}>
                            BIM
                            <span className={classes.colorText}> Services</span>
                        </h1>
                        <p className={classes.minititle}>
                            beyond information modeling..
                        </p>
                    </Grid>
    </div>
</Fragment>    
  )
}