import React,{useEffect} from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import Header from './components/Home/Header';
import CadHome from './components/CadServices/CadHome';
import AOS from "aos";
import "aos/dist/aos.css";
import {ArcAppFooterDemo} from './components/Home/Footer';
import SolidGameCardDemo from './components/CadServices/CadHome';
import SolidGameCardDemobim from './components/BimServices/Bim';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100vh',
    background: '#ffffff',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundImage: `url(${process.env.PUBLIC_URL + '/assets/whitebg4.png'})`,
  },
}));



export default function App() {
  const classes = useStyles();
  useEffect(() => {
    AOS.init({
      ...AOS,
      duration:2000
    });
    AOS.refresh();
  }, []);

  return (
    <div className={classes.root}>

      <CssBaseline />
     
      <BrowserRouter>
      <Switch>
        <Route  path="/" component={Header} />
        <Route   path="/cadServices" component={CadHome} />
      </Switch>
      </BrowserRouter>
      <SolidGameCardDemo/>
      <SolidGameCardDemobim/>
      <ArcAppFooterDemo />
     
    </div>
  );
}
